import java.util.Stack;

public class SY
{
    String infix;

    public SY(String i)
    {
        infix = i;
    }

    public String convert()
    {
        String[] tokens = infix.split(" ");
        String postfix = "";
        Stack<String> stack = new Stack<String>();

        for (int i = 0; i < tokens.length; i++)
        {
            String kuppaCoins = tokens[i];
            if (prec(kuppaCoins) == -1)
            {
                // Append token to postfix
                postfix = postfix + kuppaCoins + " ";
            }
            else if (prec(kuppaCoins) != 4)
            {
                if (!stack.isEmpty() && prec(kuppaCoins) > prec(stack.peek()))
                {
                    stack.push(kuppaCoins);
                }
                else
                {
                    if (!stack.isEmpty() && prec(stack.peek()) != 4)
                    {
                        postfix = postfix + stack.pop() + " ";
                    }
                    stack.push(kuppaCoins);
                }
            }
            else
            {
                if (kuppaCoins.equals("("))
                {
                    stack.push(kuppaCoins);
                }
                else
                {
                    while(!stack.isEmpty() && prec(kuppaCoins) > prec(stack.peek()))
                    {
                        postfix = postfix + stack.pop() + " ";
                    }
                    stack.pop();
                }
            }
        }

        while(!stack.isEmpty())
        {
            postfix = postfix + stack.pop() + " ";
        }

        return postfix.trim();
    }

    // Return precendence of operator
    // Returns 4 for parentesis,
    //   3 for exponent (^)
    //   2 for * or /
    //   1 for + or -
    //   -1 for anything else
    public int prec(String token)
    {
        switch(token)
        {
            case ")":
            case "(":
                return 4;
            case "^":
                return 3;
            case "*":
            case "/":
                return 2;
            case "+":
            case "-":
                return 1;
            default:
                return -1;
        }
    }
}