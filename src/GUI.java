import acm.program.*;
import acm.graphics.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class GUI extends Program
{
    JTextField infixInput;
    JTextField postfixOutput;
    JTextField resultOutput;
    JTextField stoRCLOutput;
    JTextField aOutput;
    JTextField bOutput;
    JTextField cOutput;
    JTextField dOutput;
    JTextField eOutput;

    String tempAns = "";
    String tempVar = "";
    String tempVarA = "";
    String tempVarB = "";
    String tempVarC = "";
    String tempVarD = "";
    String tempVarE = "";

    public GUI()
    {
        start();
        setSize(370,410);
    }

    public void init()
    {
        // Create a canvas and add it to the window
        GCanvas canvas = new GCanvas();
        add(canvas);

        setTitle("Calculator");

        canvas.setBackground(Color.cyan);

        JLabel infixLabel = new JLabel("Infix");
        JLabel postfixLabel = new JLabel("Postfix");
        JLabel resultLable = new JLabel("Answer");
        JLabel stoRCLLable = new JLabel("sto/rcl");
        canvas.add(infixLabel, 20, 10);
        canvas.add(postfixLabel, 20, 40);
        canvas.add(resultLable, 20, 70);
        canvas.add(stoRCLLable, 20, 110);

        infixInput = new JTextField();
        postfixOutput = new JTextField();
        resultOutput = new JTextField();
        stoRCLOutput = new JTextField();
        canvas.add(infixInput, 70, 10);
        canvas.add(postfixOutput, 70, 40);
        canvas.add(resultOutput, 70, 70);
        canvas.add(stoRCLOutput, 65, 110);

        infixInput.setSize(200, 20);
        postfixOutput.setSize(200, 20);
        resultOutput.setSize(200, 20);
        stoRCLOutput.setSize(35, 20);

        // color change
        JButton cyanButton = new JButton("Cyan");
        JButton magentaButton = new JButton("Magenta");
        JButton yellowButton = new JButton("Yellow");
        canvas.add(cyanButton, 20, 290);
        canvas.add(magentaButton, 100, 290);
        canvas.add(yellowButton, 214, 290);
        Font comicSans = new Font("Comic Sans MS", Font.PLAIN, 18);
        cyanButton.setFont(comicSans);
        cyanButton.setSize(cyanButton.getPreferredSize());
        cyanButton.setForeground(Color.cyan);
        magentaButton.setFont(comicSans);
        magentaButton.setSize(magentaButton.getPreferredSize());
        magentaButton.setForeground(Color.magenta);
        yellowButton.setFont(comicSans);
        yellowButton.setSize(yellowButton.getPreferredSize());
        yellowButton.setForeground(Color.yellow);
        // number buttons
        JButton oneButton = new JButton("1");
        JButton twoButton = new JButton("2");
        JButton threeButton = new JButton("3");
        JButton fourButton = new JButton("4");
        JButton fiveButton = new JButton("5");
        JButton sixButton = new JButton("6");
        JButton sevenButton = new JButton("7");
        JButton eightButton = new JButton("8");
        JButton nineButton = new JButton("9");
        JButton zeroButton = new JButton("0");

        JButton storeButton = new JButton("sto");
        JButton recallButton = new JButton("rcl");
        // store and recall variable buttons
        JButton aButton = new JButton("A");
        JButton bButton = new JButton("B");
        JButton cButton = new JButton("C");
        JButton dButton = new JButton("D");
        JButton eButton = new JButton("E");
        JButton asButton = new JButton("A sto");
        JButton bsButton = new JButton("B sto");
        JButton csButton = new JButton("C sto");
        JButton dsButton = new JButton("D sto");
        JButton esButton = new JButton("E sto");

        JButton additionButton = new JButton("+");
        JButton subtractionButton = new JButton("- ");
        JButton multiplicationButton = new JButton("* ");
        JButton divisionButton = new JButton(" / ");
        JButton solveButton = new JButton("= ");
        JButton exponentButton = new JButton(" ^ ");
        JButton sqrtButton = new JButton("sqrt");
        JButton decimalButton = new JButton(" .  ");
        JButton sinButton = new JButton("  sin  ");
        JButton cosButton = new JButton(" cos ");
        JButton tanButton = new JButton("tan ");
        JButton openPButton = new JButton("( ");
        JButton closePButton = new JButton(" )");
        JButton clearButton = new JButton("   clear   ");
        JButton ansButton = new JButton("ans");
        JButton negationButton = new JButton("(-)");


        canvas.add(sevenButton, 65, 170);
        canvas.add(eightButton, 110, 170);
        canvas.add(nineButton, 155, 170);
        canvas.add(fourButton, 65, 200);
        canvas.add(fiveButton, 110, 200);
        canvas.add(sixButton, 155, 200);
        canvas.add(oneButton, 65, 230);
        canvas.add(twoButton, 110, 230);
        canvas.add(threeButton, 155, 230);
        canvas.add(zeroButton, 65, 260);

        canvas.add(aButton, 240, 140);
        canvas.add(bButton, 240, 170);
        canvas.add(cButton, 240, 200);
        canvas.add(dButton, 240, 230);
        canvas.add(eButton, 240, 260);
        canvas.add(asButton, 280, 140);
        canvas.add(bsButton, 280, 170);
        canvas.add(csButton, 280, 200);
        canvas.add(dsButton, 280, 230);
        canvas.add(esButton, 280, 260);

        canvas.add(sinButton, 20, 140);
        canvas.add(cosButton, 80, 140);
        canvas.add(tanButton, 140, 140);
        canvas.add(exponentButton, 20, 170);
        canvas.add(decimalButton, 20, 200);
        canvas.add(openPButton, 110, 260);
        canvas.add(closePButton, 155, 260);
        canvas.add(solveButton, 200, 260 );
        canvas.add(additionButton, 200, 140);
        canvas.add(subtractionButton, 200, 170);
        canvas.add(multiplicationButton, 200, 200);
        canvas.add(divisionButton, 200, 230);
        canvas.add(clearButton, 200, 110);
        canvas.add(negationButton, 20, 230);
        canvas.add(ansButton, 20, 260);
        canvas.add(storeButton, 100, 110);
        canvas.add(recallButton, 150, 110);

        addActionListeners();
    }

    public void actionPerformed(ActionEvent e)
    {
        //System.out.println(e.getActionCommand());
        String infixStr = new String("");

        if (e.getActionCommand().equals("= "))
        {
            // Get value from infixInput
            String iStr = infixInput.getText();
            // Create SY converter
            SY m = new SY(iStr);
            // convert to postfix
            String pStr = m.convert();
            // put result into postfixOutput
            postfixOutput.setText("" + pStr);
            // Get value from postfixOutput
            String pfix = postfixOutput.getText();
            // Create postfix elva
            Postfix p = new Postfix(pfix);
            double rslt = p.eval();

            // Put result into Answer box
            resultOutput.setText("" + rslt);

            tempAns = resultOutput.getText();
        }
        else if (e.getActionCommand().equals("ans"))
        {

            postfixOutput.setText("");
            infixStr = infixInput.getText() + tempAns;
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("   clear   "))
        {
            infixInput.setText("");
            postfixOutput.setText("");
            resultOutput.setText("");
        }
        // numbers
        else if (e.getActionCommand().equals("0"))
        {
            infixStr = infixInput.getText() + "0";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("1"))
        {
            infixStr = infixInput.getText() + "1";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("2"))
        {
            infixStr = infixInput.getText() + "2";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("3"))
        {
            infixStr = infixInput.getText() + "3";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("4"))
        {
            infixStr = infixInput.getText() + "4";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("5"))
        {
            infixStr = infixInput.getText() + "5";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("6"))
        {
            infixStr = infixInput.getText() + "6";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("7"))
        {
            infixStr = infixInput.getText() + "7";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("8"))
        {
            infixStr = infixInput.getText() + "8";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("9"))
        {
            infixStr = infixInput.getText() + "9";
            infixInput.setText(infixStr);
        }
        // STO & RCL Variables
        else if (e.getActionCommand().equals("rcl"))
        {
            stoRCLOutput.setText("rcl");
            postfixOutput.setText("");
            infixStr = infixInput.getText() + tempVar;
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("sto"))
        {
            stoRCLOutput.setText("sto");
            tempVar = tempAns;
        }
        else if (e.getActionCommand().equals("A"))
        {
            stoRCLOutput.setText("rcl A");
            postfixOutput.setText("");
            infixStr = infixInput.getText() + tempVarA;
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("A sto"))
        {
            stoRCLOutput.setText("sto A");
            tempVarA = tempAns;
        }
        else if (e.getActionCommand().equals("B"))
        {
            stoRCLOutput.setText("rcl B");
            postfixOutput.setText("");
            infixStr = infixInput.getText() + tempVarB;
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("B sto"))
        {
            stoRCLOutput.setText("sto B");
            tempVarB = tempAns;
        }
        else if (e.getActionCommand().equals("C"))
        {
            stoRCLOutput.setText("rcl C");
            postfixOutput.setText("");
            infixStr = infixInput.getText() + tempVarC;
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("C sto"))
        {
            stoRCLOutput.setText("sto C");
            tempVarC = tempAns;
        }
        else if (e.getActionCommand().equals("D"))
        {
            stoRCLOutput.setText("rcl D");
            postfixOutput.setText("");
            infixStr = infixInput.getText() + tempVarD;
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("D sto"))
        {
            stoRCLOutput.setText("sto D");
            tempVarD = tempAns;
        }
        else if (e.getActionCommand().equals("E"))
        {
            stoRCLOutput.setText("rcl E");
            postfixOutput.setText("");
            infixStr = infixInput.getText() + tempVarE;
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("E sto"))
        {
            stoRCLOutput.setText("sto E");
            tempVarE = tempAns;
        }
        // symb.

        else if (e.getActionCommand().equals("+"))
        {
            infixStr = infixInput.getText() + " + ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("- "))
        {
            infixStr = infixInput.getText() + " - ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("* "))
        {
            infixStr = infixInput.getText() + " * ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals(" / "))
        {
            infixStr = infixInput.getText() + " / ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals(" ^ "))
        {
            infixStr = infixInput.getText() + " ^ ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("( "))
        {
            infixStr = infixInput.getText() + "( ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals(" )"))
        {
            infixStr = infixInput.getText() + " )";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals(" .  "))
        {
            infixStr = infixInput.getText() + ".";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("(-)"))
        {
            infixStr = infixInput.getText() + "-";
            infixInput.setText(infixStr);
        }
        //trig funct
        else if (e.getActionCommand().equals("  sin  "))
        {
            infixStr = infixInput.getText() + "sin ( ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals(" cos "))
        {
            infixStr = infixInput.getText() + "cos ( ";
            infixInput.setText(infixStr);
        }
        else if (e.getActionCommand().equals("tan "))
        {
            infixStr = infixInput.getText() + "tan ( ";
            infixInput.setText(infixStr);
        }
    }
    public static void main(String[] args)
    {
        GUI g = new GUI();
    }
}